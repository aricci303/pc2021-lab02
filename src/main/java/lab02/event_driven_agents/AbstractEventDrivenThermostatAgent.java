package lab02.event_driven_agents;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.WebSocket;
import io.vertx.core.http.WebSocketConnectOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;

/**
 * Abstract base class for event-driven thermostat agents.
 * 
 * @author aricci
 *
 */
public abstract class AbstractEventDrivenThermostatAgent extends AbstractVerticle {
	
	private String roomThingURI;
	private HttpClient wsClient;
	private WebClient client;
	private Vertx vertx;
	private int port;
	
	public AbstractEventDrivenThermostatAgent(String roomThingURI, int port) {
		this.roomThingURI = roomThingURI;
		this.port = port;
		vertx = Vertx.vertx();		
		wsClient = vertx.createHttpClient();
		client = WebClient.create(vertx);
	}
		
	// actions
		
	protected void startObservingThing(Handler<Buffer> handler){
		WebSocketConnectOptions opt = new WebSocketConnectOptions();
		opt.addSubProtocol("webthing");
		opt.setHost(roomThingURI);
		opt.setPort(port);
		opt.setURI("/");
		log ("Connecting to the thing...");		
		wsClient.webSocket(opt, res -> {
			if (res.succeeded()) {
				log("Connected! Subscribing.");
				
				WebSocket ws = res.result();				
				JsonObject msg = new JsonObject();
				msg
				.put("messageType", "addEventSubscription")
				.put("data", new JsonObject());
				
				ws.writeTextMessage(msg.encodePrettily());
	
				/* handling events */
				
				ws.handler(handler);
			}
		});
	}
	
	protected String getRoomThingURI() {
		return this.roomThingURI;
	}
	
	protected void log(String msg) {
		System.out.println("[THERMOSTAT] " + msg);
	}
	
	protected Future<Double> getCurrentTemperature() {
		Future<JsonObject> val = this.readProperty("temperature");
		Future<Double> fut = val.compose(jsonObjRes -> {
			return Future.<Double>future( promise -> {
				promise.complete(jsonObjRes.getDouble("temperature"));
			});
		});
		return fut;
	}

	protected Future<String> getCurrentState() {
		Future<JsonObject> val = this.readProperty("state");
		Future<String> fut = val.compose(jsonObjRes -> {
			return Future.<String>future( promise -> {
				promise.complete(jsonObjRes.getString("state"));
			});
		});
		return fut;
	}
	
	protected void startCooling() {
		JsonObject msg = new JsonObject();
		msg.put("startCooling", new JsonObject()
				.put("input", new JsonObject()));
		this.execAction("startCooling", msg);
	}

	protected void startHeating() {
		JsonObject msg = new JsonObject();
		msg.put("startHeating", new JsonObject()
				.put("input", new JsonObject()));
		this.execAction("startHeating", msg);
	}

	protected void stopWorking() {
		JsonObject msg = new JsonObject();
		msg.put("stopWorking", new JsonObject()
				.put("input", new JsonObject()));
		this.execAction("stopWorking", msg);
	}
	
	
	// aux actions
	
	private Future<JsonObject> readProperty(String property) {
		Promise<JsonObject> prom = Promise.promise();		
		client
		  .get(port, roomThingURI, "/properties/" + property)
		  .send(ar -> {
		    if (ar.succeeded()) {
		      // Obtain response
		      HttpResponse<Buffer> response = ar.result();
		      // System.out.println("Received response with status code " + response.statusCode());
			  JsonObject res = response.bodyAsJsonObject();
			  prom.complete(res);
		    } else {
		      System.out.println("Something went wrong " + ar.cause().getMessage());
		      prom.fail(ar.cause().getMessage());
		    }
		  });
		return prom.future();
	}

	private Future<JsonObject> execAction(String action, JsonObject body) {
		Promise<JsonObject> prom = Promise.promise();		
		client
		  .post(port, roomThingURI, "/actions/" + action)
		  .sendJson(body, ar -> {
		    if (ar.succeeded()) {
			      // Obtain response
			      HttpResponse<Buffer> response = ar.result();
			      // System.out.println("Received response with status code " + response.statusCode());
				  JsonObject res = response.bodyAsJsonObject();
				  prom.complete(res);
		    } else {
		      System.out.println("Something went wrong " + ar.cause().getMessage());
		      prom.fail(ar.cause().getMessage());
		    }
		  });		
		 return prom.future();
	}
}
