package lab02.event_driven_agents;

import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

/**
 * 
 * Event-oriented simple agent whose task is to achieve a target temperature,
 * eventually reacting to temperature changes while achieving it.
 * 
 * @author aricci
 *
 */
public class ThermostatEventDrivenAgentStep1 extends AbstractEventDrivenThermostatAgent {

	private static double THRESOLD = 0.5;
	private static String IDLE = "idle";
	private static String COOLING = "cooling";
	private static String HEATING = "heating";
	private double targetGoalTemperature;

	private double currentTemperature;
	private String currentState;
	
	public ThermostatEventDrivenAgentStep1(String roomThingURI, int port, double targetGoalTemperature) {
		super(roomThingURI, port);
		this.targetGoalTemperature = targetGoalTemperature;
	}
	
	public void start() {
		log("Launched - working with room at " + getRoomThingURI() + " - temperature to achieve: " + targetGoalTemperature);		
		
		/* pro-active part */
		
		Future<Double> currentTempFut = this.getCurrentTemperature();
		Future<String> currentStateFut = this.getCurrentState();
		
		CompositeFuture.all(currentTempFut, currentStateFut).onComplete(ar -> {
			if (ar.succeeded()) {
				currentTemperature = currentTempFut.result();
				currentState = currentStateFut.result();
				doCheck();
			}
		});
		
		/* reactive part */
		 
		this.startObservingThing(msg -> {
			log("new event observed: \n" + msg.toJsonObject().encodePrettily());
			JsonObject ev = msg.toJsonObject();
			String msgType = ev.getString("messageType");
			if (msgType.equals("propertyStatus")) {
				JsonObject data = ev.getJsonObject("data");
				Double newTemperature = data.getDouble("temperature");
				String newState = data.getString("state");
				if (newTemperature != null) {
					currentTemperature = newTemperature; 
				}
				if (newState != null) {
					currentState = newState; 
				}
				
				doCheck();
			}
		});
	}

	private void doCheck() {
		log("State: current temperature: " + currentTemperature + " - state: " + currentState );
		if (currentTemperature < (targetGoalTemperature - THRESOLD) && !currentState.equals(HEATING)) {
			log("too cold: start heating...");
			startHeating();
		} else if (currentTemperature > (targetGoalTemperature + THRESOLD) && !currentState.equals(COOLING)) {
			log("too hot: start cooling...");
			startCooling();
		} else if (Math.abs(currentTemperature - targetGoalTemperature) < THRESOLD) {
			log("achieved: stop working");
			if (!currentState.equals(IDLE)) {
				stopWorking();
				try {
					this.stop();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();		
		vertx.deployVerticle(new ThermostatEventDrivenAgentStep1("localhost", 8000, 21));		
	}	

}
