package lab02.event_driven_agents;

import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

/**
 * 
 * Event-oriented simple agent whose task is to achieve and maintain a target temperature,
 * eventually reacting to temperature changes while achieving it.
 * 
 * @author aricci
 *
 */
public class ThermostatEventDrivenAgentStep3 extends AbstractEventDrivenThermostatAgent {

	private static double THRESOLD = 0.5;
	private static String IDLE = "idle";
	private static String COOLING = "cooling";
	private static String HEATING = "heating";
	private double targetGoalTemperature;
	private UserPrefPanel panel;
	private double currentTemperature;
	private String currentState;
	
	public ThermostatEventDrivenAgentStep3(String roomThingURI, int port) {
		super(roomThingURI, port);
	}
	
	public void start() {
		log("Launched - working with room at " + getRoomThingURI());		
		
		log("setting up GUI.");
		createUserGUI();
		
		this.getVertx().eventBus().consumer("gui", msg -> {
			if (msg.body().equals("started")) {
				targetGoalTemperature = panel.getCurrentUserTemperature();
				Future<Double> currentTempFut = this.getCurrentTemperature();
				Future<String> currentStateFut = this.getCurrentState();
				CompositeFuture.all(currentTempFut, currentStateFut).onComplete(ar -> {
					if (ar.succeeded()) {
						currentTemperature = currentTempFut.result();
						currentState = currentStateFut.result();
						doCheck();
					}
				});
			} else if (msg.body().equals("targetTemperatureUpdated")) {
				targetGoalTemperature = panel.getCurrentUserTemperature();
				doCheck();
			}
			
		});
		
		this.startObservingThing(msg -> {
			log("new event observed: \n" + msg.toJsonObject().encodePrettily());
			JsonObject ev = msg.toJsonObject();
			String msgType = ev.getString("messageType");
			if (msgType.equals("propertyStatus")) {
				JsonObject data = ev.getJsonObject("data");
				Double newTemperature = data.getDouble("temperature");
				String newState = data.getString("state");
				if (newTemperature != null) {
					currentTemperature = newTemperature; 
				}
				if (newState != null) {
					currentState = newState; 
				}				
				doCheck();
			}
		});
	}

	private void doCheck() {
		log("State: current temperature: " + currentTemperature + " - state: " + currentState );
		if (currentTemperature < (targetGoalTemperature - THRESOLD) && !currentState.equals(HEATING)) {
			log("too cold: start heating...");
			startHeating();
		} else if (currentTemperature > (targetGoalTemperature + THRESOLD) && !currentState.equals(COOLING)) {
			log("too hot: start cooling...");
			startCooling();
		} else if (Math.abs(currentTemperature - targetGoalTemperature) < THRESOLD) {
			log("achieved: stop working");
			if (!currentState.equals(IDLE)) {
				stopWorking();
			}
		}
	}
	
	protected void createUserGUI() {
		panel = new UserPrefPanel(this.getVertx().eventBus());
		panel.display();
	}
	
	protected double getUserTargetTemperature() {
		return panel.getCurrentUserTemperature();
	}
		
	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();		
		vertx.deployVerticle(new ThermostatEventDrivenAgentStep3("localhost", 8000));		
	}	

}
