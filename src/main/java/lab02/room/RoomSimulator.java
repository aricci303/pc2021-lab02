package lab02.room;

import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.SwingUtilities;

/**
 * Simulating the behaviour of the room (temperature changes)
 * 
 * @author aricci
 *
 */
public class RoomSimulator {

	private RoomThing model;
	private final ScheduledExecutorService scheduler =
		     Executors.newScheduledThreadPool(1);
	private Optional<ScheduledFuture<?>> handle = Optional.empty();
	private RoomSimulatorPanel panel;
	
	public RoomSimulator(RoomThing model) {
		this.model = model;
	    model.setState("idle");
	    model.setTemperature(21);
	    panel = new RoomSimulatorPanel(model);
	    SwingUtilities.invokeLater(() -> {
	    		panel.setVisible(true);
	    });
	}
	
	public void startHeating(int period) {
	    if (handle.isPresent()) {
	    		handle.get().cancel(true);
	    }
	    model.setState("heating");
		handle = Optional.of(scheduler.scheduleAtFixedRate(() -> {
			double t = model.getTemperature();
			model.setTemperature(t + 0.1);	
			panel.updateTemp();
	     }, 0, period, TimeUnit.MILLISECONDS));
	}
	
	public void startCooling(int period) {
	    if (handle.isPresent()) {
    			handle.get().cancel(true);
	    }
	    model.setState("cooling");
	    handle = Optional.of(scheduler.scheduleAtFixedRate(() -> {
	    		double t = model.getTemperature();
	    		model.setTemperature(t - 0.1);	
			panel.updateTemp();
	    }, 0, period, TimeUnit.MILLISECONDS));
	}
	
	public void stopWorking() {
	   if (handle.isPresent()) {
	    		handle.get().cancel(true);
	    		handle = Optional.empty();
	    	    model.setState("idle");
	   }
	}
}
