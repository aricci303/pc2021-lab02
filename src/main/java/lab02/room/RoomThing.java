package lab02.room;

import java.io.IOException;
import java.util.Arrays;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONObject;

import io.webthings.webthing.Action;
import io.webthings.webthing.Property;
import io.webthings.webthing.Thing;
import io.webthings.webthing.Value;
import io.webthings.webthing.WebThingServer;

/**
 * A toy Room Thing, providing only capabilities
 * about temperature sensing and dumb air conditioning
 *  
 * @author aricci
 *
 */
public class RoomThing extends Thing {

	// to expose an API
    private WebThingServer server;

    // simulating the room behaviour
    private RoomSimulator roomSimulator;
    
	public RoomThing(String id, String desc) {
        super("urn:dev:ops:"+id,
        				desc,
        				new JSONArray(Arrays.asList("TemperatureSensing","DumbAirConditioner")),
                		"A room thing");

        /* temperature property */
        
		JSONObject tempDescription = new JSONObject();
		tempDescription.put("@type", "TemperatureProperty");
		tempDescription.put("title", "temperature");
		tempDescription.put("type", "number");
		tempDescription.put("description", "temperature");
		addProperty(new Property(this,
								"temperature",
								new Value(0.0),
								tempDescription));
		
		/* HVAC state property: idle, heating, cooling */
		
		JSONObject statusDescription = new JSONObject();
		statusDescription.put("@type", "HVACStatusProperty");
		statusDescription.put("title", "status");
		statusDescription.put("type", "string");
		statusDescription.put("description", "status");
		addProperty(new Property(this,
								"state",
								new Value("idle"),
								statusDescription));
		
		/* start heating action */
		
        JSONObject startHeatingMetadata = new JSONObject();
        JSONObject startHeatingInput = new JSONObject();
        JSONObject startHeatingProperties = new JSONObject();
        startHeatingMetadata.put("title", "startHeating");
        startHeatingMetadata.put("description", "Start heating.");
        startHeatingMetadata.put("type", "object");
        startHeatingInput.put("properties", startHeatingProperties);
        startHeatingMetadata.put("input", startHeatingInput);
        addAvailableAction("startHeating", startHeatingMetadata, StartHeatingAction.class);

        /* start cooling action */
        
        JSONObject startCoolingMetadata = new JSONObject();
        JSONObject startCoolingInput = new JSONObject();
        JSONObject startCoolingProperties = new JSONObject();
        startCoolingMetadata.put("title", "startCooling");
        startCoolingMetadata.put("description", "Start cooling.");
        startCoolingMetadata.put("type", "object");
        startCoolingInput.put("properties", startCoolingProperties);
        startCoolingMetadata.put("input", startCoolingInput);
        addAvailableAction("startCooling", startCoolingMetadata, StartCoolingAction.class);

        /* stop working */
        
        JSONObject stopWorkingMetadata = new JSONObject();
        JSONObject stopWorkingInput = new JSONObject();
        JSONObject stopWorkingProperties = new JSONObject();
        stopWorkingMetadata.put("title", "stopWorking");
        stopWorkingMetadata.put("description", "Stop working.");
        stopWorkingMetadata.put("type", "object");
        stopWorkingInput.put("properties", stopWorkingProperties);
        stopWorkingMetadata.put("input", stopWorkingInput);
        addAvailableAction("stopWorking", stopWorkingMetadata, StopWorkingAction.class);
        
        
    }

	/**
	 * To deploy the room thing.
	 * 
	 * @throws Exception
	 */
	public void deploy() throws Exception {
		log("Creating the room simulator..");
        roomSimulator = new RoomSimulator(this);
		log("Deploying the thing...");
		server = new WebThingServer(new WebThingServer.SingleThing(this), 8000);
        Runtime.getRuntime().addShutdownHook(new Thread() {
                public void run() {
                    server.stop();
                }
        });

        server.start(false);
		log("API ready.");
	}

	private void log(String msg) {
		System.out.println("[ROOM THING] " + msg);
	}
	
	/* used by the simulator and the API */
	
	synchronized void setTemperature(double temperature) {
		try {
			setProperty("temperature", temperature);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	synchronized double getTemperature() {
        return (double) getProperty("temperature");
	}
	
	synchronized void setState(String status)  {
		try {
			setProperty("state", status);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	synchronized String getState() {
        return (String) getProperty("state");
	}
	
	void startHeating() {
		this.roomSimulator.startHeating(100);
	}

	void startCooling() {
		this.roomSimulator.startCooling(100);
	}

	void stopWorking() {
		this.roomSimulator.stopWorking();
	}
	
	
	public static class StartHeatingAction extends Action {
        public StartHeatingAction(Thing thing, JSONObject input) {
            super(UUID.randomUUID().toString(), thing, "startHeating", input);
        }

        @Override
        public void performAction() {
        	((RoomThing) this.getThing()).startHeating();
        }
    }

	public static class StartCoolingAction extends Action {
        public StartCoolingAction(Thing thing, JSONObject input) {
            super(UUID.randomUUID().toString(), thing, "startCooling", input);
        }

        @Override
        public void performAction() {
        	((RoomThing) this.getThing()).startCooling();
        }
    }

	public static class StopWorkingAction extends Action {
        public StopWorkingAction(Thing thing, JSONObject input) {
            super(UUID.randomUUID().toString(), thing, "stopWorking", input);
        }

        @Override
        public void performAction() {
        	((RoomThing) this.getThing()).stopWorking();
        }
    }
	
}
