package lab02.thread_based_agents;

/**
 * 
 * Thread based agent whose task is to achieve a target temperature,
 * eventually reacting to temperature changes while achieving it.
 * 
 * @author aricci
 *
 */
public class ThermostatThreadBasedAgentStep1 extends AbstractThreadBasedThermostatAgent {

	private static double THRESOLD = 0.5;
	private static long AGENT_CYCLE_PERIOD = 500;
	private static String IDLE = "idle";
	private static String COOLING = "cooling";
	private static String HEATING = "heating";
	
	private double targetGoalTemperature;
	
	public ThermostatThreadBasedAgentStep1(String roomThingURI, int port, double targetGoalTemperature) {
		super(roomThingURI, port);
		this.targetGoalTemperature = targetGoalTemperature;
	}
	
	public void run() {
		log("Launched - working with room at " + getRoomThingURI() + " - temperature to achieve: " + targetGoalTemperature);		
		log("Connecting to the room thing...");
		while (true) {
			double currentTemp = this.getCurrentTemperature();
			String state = this.getCurrentState();
			log("State: current temperature: " + currentTemp + " - state: " + state );
			if (currentTemp < (targetGoalTemperature - THRESOLD) && !state.equals(HEATING)) {
				log("too cold: start heating...");
				startHeating();
			} else if (currentTemp > (targetGoalTemperature + THRESOLD) && !state.equals(COOLING)) {
				log("too hot: start cooling...");
				startCooling();
			} else if (Math.abs(currentTemp - targetGoalTemperature) < THRESOLD) {
				log("achieved: stop working");
				if (!state.equals(IDLE)) {
					stopWorking();
				}
				break;
			}
			waitTimeInMs(AGENT_CYCLE_PERIOD);
		}
		log("GOAL ACHIEVED.");
	}
	
	
	public static void main(String[] args) {
		new ThermostatThreadBasedAgentStep1("localhost", 8000, 17).start();
	}	
}
