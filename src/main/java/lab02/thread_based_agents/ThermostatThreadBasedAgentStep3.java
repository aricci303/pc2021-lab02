package lab02.thread_based_agents;

/**
 * 
 * Thread based agent whose task is to achieve and maintain a target temperature,
 * which can be specified and changed dynamically by a user through a GUI.
 * 
 * @author aricci
 *
 */
public class ThermostatThreadBasedAgentStep3 extends AbstractThreadBasedThermostatAgent {

	private static double THRESOLD = 0.5;
	private static String IDLE = "idle";
	private static String COOLING = "cooling";
	private static String HEATING = "heating";
	private double targetGoalTemperature;
	private static long AGENT_CYCLE_PERIOD = 500;
	private static long AGENT_CYCLE_LONG_PERIOD = 1000;
	private 	UserPrefPanel panel;

	
	public ThermostatThreadBasedAgentStep3(String roomThingURI, int port, double targetGoalTemperature) {
		super(roomThingURI, port);
		this.targetGoalTemperature = targetGoalTemperature;
	}
	
	public void run() {
		log("launched - working with room thing at " + getRoomThingURI());
		
		log("setting up GUI.");
		createUserGUI();
		
		log("waiting for user start.");
		waitForUserStart();
		
		while (true) {
			double currentTemp = this.getCurrentTemperature();
			String state = this.getCurrentState();
			targetGoalTemperature = this.getUserTargetTemperature();
					
			log("Idle (State: current temperature: " + currentTemp + " - state: " + state +" - target temp: " + targetGoalTemperature + ")" );
			if (Math.abs(currentTemp - targetGoalTemperature) > THRESOLD) {
				boolean tempAchieved = false;
				while (!tempAchieved) {
					currentTemp = this.getCurrentTemperature();
					state = this.getCurrentState();
					targetGoalTemperature = this.getUserTargetTemperature();

					log("Acting (State: current temperature: " + currentTemp + " - state: " + state +" - target temp: " + targetGoalTemperature + ")" );
					if (currentTemp < (targetGoalTemperature - THRESOLD) && !state.equals(HEATING)) {
						log("too cold: start heating...");
						startHeating();
					} else if (currentTemp > (targetGoalTemperature + THRESOLD) && !state.equals(COOLING)) {
						log("too hot: start cooling...");
						startCooling();
					} else if (Math.abs(currentTemp - targetGoalTemperature) < THRESOLD) {
						log("achieved: stop working");
						if (!state.equals(IDLE)) {
							stopWorking();
						}
						tempAchieved = true;
					}
					waitTimeInMs(AGENT_CYCLE_PERIOD);
				}
			}
			waitTimeInMs(AGENT_CYCLE_LONG_PERIOD);
		}
	}
	
	// actions
	
	protected void createUserGUI() {
		panel = new 	UserPrefPanel();
		panel.display();
	}
	
	protected double getUserTargetTemperature() {
		return panel.getCurrentUserTemperature();
	}
	
	protected void waitForUserStart() {
		try {
			panel.waitForStart();
		} catch (Exception ex) {}
	}
	
	public static void main(String[] args) {
		new ThermostatThreadBasedAgentStep3("localhost", 8000, 21).start();
	}	

}
