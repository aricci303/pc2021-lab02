package lab02.thread_based_agents;

import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.*;
import javax.swing.event.ChangeEvent;

class UserPrefPanel extends JFrame {		
	
	private JTextField tempValue;
	private JSlider temp;
	private JButton start;
	private int currentUserPref = 18;
	private boolean started = false;
	
	public UserPrefPanel(){
		setTitle("..:: User Pref Panel ::..");
		setSize(400,160);
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		setContentPane(mainPanel);
		
		JPanel temperature = new JPanel();
		temperature.setLayout(new BoxLayout(temperature, BoxLayout.Y_AXIS));

		JPanel temperature1 = new JPanel();
		temperature1.setLayout(new BoxLayout(temperature1, BoxLayout.X_AXIS));
		
		tempValue = new JTextField(5);
		tempValue.setText("" + currentUserPref);
		tempValue.setSize(100, 30);
		tempValue.setMinimumSize(tempValue.getSize());
		tempValue.setMaximumSize(tempValue.getSize());
		tempValue.setEditable(false);
		
		temperature1.add(new JLabel("User pref temperature:"));
		temperature1.add(Box.createRigidArea(new Dimension(0,5)));
		temperature1.add(tempValue);
		
		temp = new JSlider(JSlider.HORIZONTAL, 5, 45, currentUserPref);
		temp.setSize(300, 60);
		temp.setMinimumSize(temp.getSize());
		temp.setMaximumSize(temp.getSize());
		temp.setMajorTickSpacing(10);
		temp.setMinorTickSpacing(1);
		temp.setPaintTicks(true);
		temp.setPaintLabels(true);
		
		temp.addChangeListener((ChangeEvent ev) -> {
			this.updateUserTargetTemperature(temp.getValue());
			tempValue.setText("" + temp.getValue());
		});

		temperature.add(temperature1);
		temperature.add(temp);
		
		start = new JButton("start");
		start.addActionListener((ActionEvent ev) -> {
			notifyUserStart();
		});	
		
		mainPanel.add(temperature);
		mainPanel.add(start);
		
	}
	
	public synchronized int getCurrentUserTemperature() {
		return this.currentUserPref;
	}
	
	private synchronized void updateUserTargetTemperature(int temp) {
		this.currentUserPref = temp;
	}

	private synchronized void notifyUserStart() {
		started = true;
		notifyAll();
	}
	
	public synchronized void waitForStart() throws InterruptedException {
		while (!started) {
			wait();
		}
	}
	
	public void display() {
		SwingUtilities.invokeLater(() -> {
			this.setVisible(true);
		});
	}
	
}
