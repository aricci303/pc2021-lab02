package lab02.thread_based_agents;

import java.util.concurrent.Semaphore;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;

/**
 * Abstract base class for thread-based thermostat agent
 * @author aricci
 *
 */
public abstract class AbstractThreadBasedThermostatAgent extends Thread {
	
	private String roomThingURI;
	private WebClient client;
	private Vertx vertx;
	private int port;
	
	public AbstractThreadBasedThermostatAgent(String roomThingURI, int port) {
		this.roomThingURI = roomThingURI;
		this.port = port;
		vertx = Vertx.vertx();		
		client = WebClient.create(vertx);
	}
	
	
	// actions
	
	protected String getRoomThingURI() {
		return this.roomThingURI;
	}
	
	protected void log(String msg) {
		System.out.println("[THERMOSTAT] " + msg);
	}
	
	protected void waitTimeInMs(long dt) {
		try {
			Thread.sleep(dt);
		} catch (Exception ex) {};
	}
	
	protected double getCurrentTemperature() {
		JsonObject obj = this.readProperty("temperature");
		return obj.getDouble("temperature");
	}

	protected String getCurrentState() {
		JsonObject obj = this.readProperty("state");
		return obj.getString("state");
	}
	
	protected void startCooling() {
		JsonObject msg = new JsonObject();
		msg.put("startCooling", new JsonObject()
				.put("input", new JsonObject()));
		this.execAction("startCooling", msg);
	}

	protected void startHeating() {
		JsonObject msg = new JsonObject();
		msg.put("startHeating", new JsonObject()
				.put("input", new JsonObject()));
		this.execAction("startHeating", msg);
	}

	protected void stopWorking() {
		JsonObject msg = new JsonObject();
		msg.put("stopWorking", new JsonObject()
				.put("input", new JsonObject()));
		this.execAction("stopWorking", msg);
	}
	
	
	// aux actions
	
	private JsonObject readProperty(String property) {
		final Semaphore ready = new Semaphore(0);
		Holder<JsonObject> result = new Holder();
		client
		  .get(port, roomThingURI, "/properties/" + property)
		  .send(ar -> {
		    if (ar.succeeded()) {
		      // Obtain response
		      HttpResponse<Buffer> response = ar.result();
		      // System.out.println("Received response with status code " + response.statusCode());
			  JsonObject res = response.bodyAsJsonObject();
			  result.set(res);
			  ready.release();
		    } else {
		      System.out.println("Something went wrong " + ar.cause().getMessage());
			  ready.release();
		    }
		  });
		
		try {
			ready.acquire();
			return result.get();
		} catch (Exception ex) {
			return null;
		}
	}

	private JsonObject execAction(String action, JsonObject body) {
		final Semaphore ready = new Semaphore(0);
		Holder<JsonObject> result = new Holder();
		client
		  .post(port, roomThingURI, "/actions/" + action)
		  .sendJson(body, ar -> {
		    if (ar.succeeded()) {
			      // Obtain response
			      HttpResponse<Buffer> response = ar.result();
			      // System.out.println("Received response with status code " + response.statusCode());
				  JsonObject res = response.bodyAsJsonObject();
				  result.set(res);
				  ready.release();
			    } else {
			      System.out.println("Something went wrong " + ar.cause().getMessage());
				  ready.release();
			    }
			  });
			
		try {
			ready.acquire();
			return result.get();
		} catch (Exception ex) {
			return null;
		}
	}
}
